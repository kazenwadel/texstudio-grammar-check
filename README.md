# TeXstudio Grammar Check

## Languagetool
Languagetool is an online Correction Tool that allows autocorrection using browser plugins.

It can also be run on a local java http server to connect with TeXstudio free of charge.

Follow the instructions under: [Languagetool](https://dev.languagetool.org/http-server)

Its not as good as grammarly, but can correct the most grave mistakes.
